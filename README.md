# 物流管理

#### 介绍
物流管理系统，简单的增删改查，模块可以自己修改，具有AOP日志管理,因为是ssm项目，要使用Tomcat，配置文件那些自己去找，改成自己的
#### 软件架构
ssm架构，mysql


#### 安装教程

1.  克隆到本地
2.  配置数据库，登录用户
![输入图片说明](src/main/webapp/img/shuju_re.png)
3.  打开为Maven项目

#### 使用说明

1.  简单的增删改查
2.  可以自己添加新模块
3.  xxxx
页面截图
![输入图片说明](src/main/webapp/img/geren_re.png)
![输入图片说明](src/main/webapp/img/logs_re.png)
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


