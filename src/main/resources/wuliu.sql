/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.53 : Database - testdb
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE = ''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS */`testdb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `testdb`;

/*Table structure for table `sys_cp` */

DROP TABLE IF EXISTS `sys_cp`;

CREATE TABLE `sys_cp`
(
    `id`     bigint(20) NOT NULL AUTO_INCREMENT,
    `cpName` varchar(50) DEFAULT NULL,
    `cpDesc` varchar(50) DEFAULT NULL,
    `cpSend` varchar(50) DEFAULT NULL,
    `cpRec`  varchar(50) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2000061011
  DEFAULT CHARSET = utf8;

/*Data for the table `sys_cp` */

insert into `sys_cp`(`id`, `cpName`, `cpDesc`, `cpSend`, `cpRec`)
values (2000061001, '顺丰快递', '华为手机(贵重物品)', '华为官方旗舰店', '梦梦奈'),
       (2000061002, '韵达快递', '雨伞', '苏宁易购官方旗舰店', '卡斯塔利亚'),
       (2000061003, '中通快递', '小米平板5pro(贵重物品)', '小米官方旗舰店', '琉璃'),
       (2000061004, '圆通快递', '小夜灯', '夜月官方旗舰店', '卡斯塔莉莉'),
       (2000061005, '邮政快递', '三只松鼠零食大礼包', '百草味官方旗舰店', '阿尔法零'),
       (2000061006, '韵达快递', '喜之郎果冻', '喜之郎官方旗舰店', '摩耶'),
       (2000061007, '百世汇通', '养乐多', '养乐多官方旗舰店', '诺诺纳'),
       (2000061008, '天天快递', '魔术扑克牌', '苏宁易购官方旗舰店', '胡桃'),
       (2000061009, '天天快递', '枕头', '萌羽官方旗舰店', '爱莉安娜'),
       (2000061010, '中通快递', '友基Y160F数位屏', '友基旗舰店', '羲和');

/*Table structure for table `sys_log` */

DROP TABLE IF EXISTS `sys_log`;

CREATE TABLE `sys_log`
(
    `id`           int(3)    NOT NULL AUTO_INCREMENT COMMENT 'id',
    `username`     varchar(10)        DEFAULT NULL COMMENT '访问的用户名',
    `visitTimeStr` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '访问时间',
    `method`       varchar(20)        DEFAULT NULL COMMENT '使用方法',
    PRIMARY KEY (`id`)
) ENGINE = MyISAM
  AUTO_INCREMENT = 27
  DEFAULT CHARSET = utf8
  CHECKSUM = 1
  DELAY_KEY_WRITE = 1
  ROW_FORMAT = DYNAMIC;

/*Data for the table `sys_log` */

insert into `sys_log`(`id`, `username`, `visitTimeStr`, `method`)
values (1, '管理员', '2022-06-13 11:30:07', '新增一位分拣人员'),
       (2, '华云', '2022-06-13 11:30:18', '新增一位运输人员'),
       (3, '管理员', '2022-06-13 11:28:59', '删除一个用户'),
       (4, '吴幽', '2022-06-13 11:30:00', '新增一位分拣人员'),
       (5, '管理员', '2022-06-13 16:24:00', '删除一条车辆维修记录'),
       (6, '管理员', '2022-06-13 16:25:24', '添加一个用户'),
       (7, '管理员', '2022-06-13 16:35:24', '添加一个用户'),
       (8, '管理员', '2022-06-17 09:45:13', '添加一个角色'),
       (9, '管理员', '2022-06-17 09:45:16', '删除一个角色'),
       (10, '管理员', '2022-06-18 08:35:13', '删除一个用户'),
       (11, '管理员', '2022-06-18 08:39:51', '添加一个用户'),
       (12, '管理员', '2022-06-18 08:40:16', '删除一个用户'),
       (13, '管理员', '2022-06-18 08:40:56', '添加一个角色'),
       (14, '管理员', '2022-06-18 08:41:01', '删除一个角色'),
       (15, '管理员', '2022-06-18 08:42:59', '添加一条订单信息'),
       (16, '管理员', '2022-06-18 08:43:23', '删除一条订单信息'),
       (17, '管理员', '2022-06-18 08:43:26', '删除一条订单信息'),
       (18, '管理员', '2022-06-18 08:52:02', '删除一条车辆维修记录'),
       (19, '管理员', '2022-06-18 09:42:35', '添加一个用户'),
       (20, '管理员', '2022-06-18 09:42:49', '删除一个用户'),
       (21, '管理员', '2022-06-18 09:43:14', '添加一个角色'),
       (22, '管理员', '2022-06-18 09:43:18', '删除一个角色'),
       (23, '管理员', '2022-06-18 09:44:03', '添加一条订单信息'),
       (24, '管理员', '2022-06-18 09:44:08', '删除一条订单信息'),
       (25, '管理员', '2022-06-18 09:44:26', '删除一条车辆维修记录'),
       (26, '管理员', '2022-06-18 09:45:53', '删除一个用户');

/*Table structure for table `sys_repair` */

DROP TABLE IF EXISTS `sys_repair`;

CREATE TABLE `sys_repair`
(
    `CarNumber`    varchar(10) NOT NULL COMMENT '车牌号',
    `RepairAdress` varchar(10)    DEFAULT NULL COMMENT '所维修公司',
    `RepairTime`   varchar(30)    DEFAULT NULL COMMENT '维修时间',
    `RepairPrice`  decimal(10, 0) DEFAULT NULL COMMENT '维修费用',
    `ToRepair`     varchar(10)    DEFAULT NULL COMMENT '送修人',
    UNIQUE KEY `CarNumber` (`CarNumber`)
) ENGINE = MyISAM
  AUTO_INCREMENT = 9
  DEFAULT CHARSET = utf8;

/*Data for the table `sys_repair` */

insert into `sys_repair`(`CarNumber`, `RepairAdress`, `RepairTime`, `RepairPrice`, `ToRepair`)
values ('川A.JM123', '捷途汽车修理厂', '2022-04-08', '300', '华云'),
       ('川A.JM589', '岷江汽车修理厂', '2022-04-26', '100', '黎萧'),
       ('川A.JP847', '福达汽车修理厂', '2022-05-8', '200', '韫轩'),
       ('川A.MY745', '嘉鑫汽车修理厂', '2022-05-16', '100', '华云'),
       ('川Q.JMFJG', '青松汽车修理厂', '2022-05-18', '500', '韫轩'),
       ('川Q.IH726', '启点汽车修理厂', '2022-05-30', '400', '黎萧'),
       ('川A.MP498', '永兴汽车修理厂', '2022-06-20', '390', '韫轩');

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role`
(
    `id`       bigint(20) NOT NULL AUTO_INCREMENT,
    `roleName` varchar(50) DEFAULT NULL,
    `roleDesc` varchar(50) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8;

/*Data for the table `sys_role` */

insert into `sys_role`(`id`, `roleName`, `roleDesc`)
values (1, '系统管理员', '负责维护该系统数据'),
       (2, 'A转运中心站长', '负责A转运中心的管理'),
       (3, 'B转运中心站长', '负责B转运中心的管理'),
       (4, '运输人员', '运送货物'),
       (5, '普通分拣员', '对货物进行分拣');

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user`
(
    `id`       bigint(20) NOT NULL AUTO_INCREMENT,
    `username` varchar(50) DEFAULT NULL,
    `email`    varchar(50) DEFAULT NULL,
    `password` varchar(80) DEFAULT NULL,
    `phoneNum` varchar(20) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8
  CHECKSUM = 1
  DELAY_KEY_WRITE = 1
  ROW_FORMAT = DYNAMIC;

/*Data for the table `sys_user` */

insert into `sys_user`(`id`, `username`, `email`, `password`, `phoneNum`)
values (1, '管理员', 'admin@163.com', '123', '13888888888'),
       (2, '华云', 'huayun@qq.com', '123', '13999999999'),
       (3, '吴幽', 'wuyou@qq.com', '123', '15187576342'),
       (4, '韫轩', 'yunxuan@163.com', '123456', '15198475372'),
       (5, '黎萧', 'lixiao@qq.com', '123', '18758458746');

/*Table structure for table `sys_user_role` */

DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role`
(
    `userId` bigint(20) NOT NULL,
    `roleId` bigint(20) NOT NULL,
    PRIMARY KEY (`userId`, `roleId`),
    KEY `roleId` (`roleId`),
    CONSTRAINT `sys_user_role_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `sys_user` (`id`),
    CONSTRAINT `sys_user_role_ibfk_2` FOREIGN KEY (`roleId`) REFERENCES `sys_role` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

/*Data for the table `sys_user_role` */

insert into `sys_user_role`(`userId`, `roleId`)
values (1, 1),
       (2, 2),
       (3, 3),
       (4, 4),
       (5, 4),
       (4, 5),
       (5, 5);

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;
