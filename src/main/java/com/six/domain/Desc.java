package com.six.domain;

public class Desc {
    private String userDel = "删除一个用户";
    private String userAdd = "添加一个用户";
    private String roleAdd = "添加一个角色";
    private String roleDel = "删除一个角色";
    private String repairDel = "删除一条车辆维修记录";
    private String cpDel = "删除一条订单信息";
    private String cpAdd = "添加一条订单信息";

    public String getUserDel() {
        return userDel;
    }

    public String getUserAdd() {
        return userAdd;
    }

    public String getRoleAdd() {
        return roleAdd;
    }

    public String getRoleDel() {
        return roleDel;
    }

    public String getRepairDel() {
        return repairDel;
    }

    public String getCpDel() {
        return cpDel;
    }

    public String getCpAdd() {
        return cpAdd;
    }
}
