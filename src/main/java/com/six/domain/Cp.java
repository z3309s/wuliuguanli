package com.six.domain;

public class Cp {

    private Integer id;
    private String cpName;
    private String cpDesc;
    private String cpSend;
    private String cpRec;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCpName() {
        return cpName;
    }

    public void setCpName(String cpName) {
        this.cpName = cpName;
    }

    public String getCpDesc() {
        return cpDesc;
    }

    public void setCpDesc(String cpDesc) {
        this.cpDesc = cpDesc;
    }

    public String getCpSend() {
        return cpSend;
    }

    public void setCpSend(String cpSend) {
        this.cpSend = cpSend;
    }

    public String getCpRec() {
        return cpRec;
    }

    public void setCpRec(String cpRec) {
        this.cpRec = cpRec;
    }

    @Override
    public String toString() {
        return "Cp{" +
                "id=" + id +
                ", cpName='" + cpName + '\'' +
                ", cpDesc='" + cpDesc + '\'' +
                ", cpSend='" + cpSend + '\'' +
                ", cpRec='" + cpRec + '\'' +
                '}';
    }
}
