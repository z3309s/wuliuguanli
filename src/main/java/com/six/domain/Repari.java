package com.six.domain;

public class Repari {
    private String CarNumber;
    private String RepairAdress;
    private String RepairTime;
    private String RepairPrice;
    private String ToRepair;

    public String getCarNumber() {
        return CarNumber;
    }

    public void setCarNumber(String carNumber) {
        CarNumber = carNumber;
    }

    public String getRepairAdress() {
        return RepairAdress;
    }

    public void setRepairAdress(String repairAdress) {
        RepairAdress = repairAdress;
    }

    public String getRepairTime() {
        return RepairTime;
    }

    public void setRepairTime(String repairTime) {
        RepairTime = repairTime;
    }

    public String getRepairPrice() {
        return RepairPrice;
    }

    public void setRepairPrice(String repairPrice) {
        RepairPrice = repairPrice;
    }

    public String getToRepair() {
        return ToRepair;
    }

    public void setToRepair(String toRepair) {
        ToRepair = toRepair;
    }

    @Override
    public String toString() {
        return "Repari{" +
                "CarNumber=" + CarNumber +
                ", RepairAdress='" + RepairAdress + '\'' +
                ", RepairTime='" + RepairTime + '\'' +
                ", RepairPrice='" + RepairPrice + '\'' +
                ", ToRepair='" + ToRepair + '\'' +
                '}';
    }
}
