package com.six.utils;

import com.six.domain.Desc;
import com.six.domain.User;
import com.six.service.LogService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Aspect
@Component
public class LogAspect {
    @Autowired
    LogService logService;

    @Pointcut("execution(public * com.six.controller.UserController.del(..))")
    public void userdel() {
    }

    @Pointcut("execution(public * com.six.controller.UserController.save(..))")
    public void useradd() {
    }

    @Pointcut("execution(public * com.six.controller.RoleController.save(..))")
    public void roleadd() {
    }

    @Pointcut("execution(public * com.six.controller.CPGLController.del(..))")
    public void cpgldel() {
    }

    @Pointcut("execution(public * com.six.controller.CPGLController.save(..))")
    public void cpgladd() {
    }

    @Pointcut("execution(public * com.six.controller.CarController.del(..))")
    public void repairdel() {
    }

    @Pointcut("execution(public * com.six.controller.RoleController.del(..))")
    public void roledel() {
    }


    @After(value = "userdel()")
    public void userdel(JoinPoint joinPoint) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        HttpSession session = request.getSession();
        Desc desc = new Desc();
        System.out.println("AOP测试");
        User user = (User) session.getAttribute("user");
        logService.addLog(user.getUsername(), desc.getUserDel());
    }

    @After(value = "useradd()")
    public void useradd(JoinPoint joinPoint) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        HttpSession session = request.getSession();
        Desc desc = new Desc();
        User user = (User) session.getAttribute("user");
        logService.addLog(user.getUsername(), desc.getUserAdd());
    }

    @After(value = "roleadd()")
    public void roleadd(JoinPoint joinPoint) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        HttpSession session = request.getSession();
        Desc desc = new Desc();
        User user = (User) session.getAttribute("user");
        logService.addLog(user.getUsername(), desc.getRoleAdd());
    }

    @After(value = "cpgldel()")
    public void cpgldel(JoinPoint joinPoint) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        HttpSession session = request.getSession();
        Desc desc = new Desc();
        User user = (User) session.getAttribute("user");
        logService.addLog(user.getUsername(), desc.getCpDel());
    }

    @After(value = "cpgladd()")
    public void cpglAdd(JoinPoint joinPoint) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        HttpSession session = request.getSession();
        Desc desc = new Desc();
        User user = (User) session.getAttribute("user");
        logService.addLog(user.getUsername(), desc.getCpAdd());
    }

    @After(value = "repairdel()")
    public void repairDel(JoinPoint joinPoint) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        HttpSession session = request.getSession();
        Desc desc = new Desc();
        User user = (User) session.getAttribute("user");
        logService.addLog(user.getUsername(), desc.getRepairDel());
    }

    @After(value = "roledel()")
    public void roleDel(JoinPoint joinPoint) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();
        HttpSession session = request.getSession();
        Desc desc = new Desc();
        User user = (User) session.getAttribute("user");
        logService.addLog(user.getUsername(), desc.getRoleDel());
    }

}
