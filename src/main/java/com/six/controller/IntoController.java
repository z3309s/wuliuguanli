package com.six.controller;

import com.six.domain.Role;
import com.six.domain.User;
import com.six.service.RoleService;
import com.six.service.UserService;
import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class IntoController {
    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;

    @RequestMapping("/perduct/list")
    public ModelAndView per() {
        ModelAndView modelAndView = new ModelAndView();
        List<Role> roleList = roleService.list();
        //设置模型
        modelAndView.addObject("roleList", roleList);
        //设置视图
        modelAndView.setViewName("perduct-list");
        System.out.println(roleList);
        return modelAndView;
    }

    @RequestMapping("/login.do")
    @ResponseBody
    public Map<String, Object> login(@Validated User user, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws UnsupportedEncodingException {
        Map<String, Object> map = new HashMap<String, Object>();
        boolean a = userService.login(user);

        if (a == false) {//验证失败
            map.put("code", 500);
            map.put("msg", "用户名或密码错误");
            return map;
        } else if (!CaptchaUtil.ver(user.getVerity(), request)) {
            CaptchaUtil.clear(request);//清楚session中的验证码
            map.put("code", 500);
            map.put("msg", "验证码错误");
            return map;


        } else {
            ////将当前用户信息存入session
            setSessionUserInfo(user, request.getSession());
            //将当前用户信息存入cookie
            setCookieUser(request, response, session);
            map.put("code", 200);
            map.put("msg", "登陆成功");
            return map;
        }

    }

    private void setCookieUser(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws UnsupportedEncodingException {
        User user = (User) session.getAttribute("user");
        Cookie cookie = new Cookie("user", URLEncoder.encode(user.getUsername(), "utf-8"));
        cookie.setMaxAge(60 * 60 * 24 * 1);   // 设置cookie的有效期为1天
        response.addCookie(cookie);
    }

    private void setSessionUserInfo(User user, HttpSession session) {
        session.setAttribute("user", user);
    }

    @RequestMapping("/register.do")
    @ResponseBody
    public Map<String, Object> register(User user) {
        Map<String, Object> map = new HashMap<String, Object>();
        boolean userIsExisted = userService.UserIsExisted(user);
        if (userIsExisted) {
            map.put("msg", "用户已存在");
        } else {
            int i = userService.addUser(user);
            if (i > 0) {
                map.put("code", 200);
            }
        }
        return map;
    }
}
