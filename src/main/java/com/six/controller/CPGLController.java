package com.six.controller;

import com.six.domain.Cp;
import com.six.domain.User;
import com.six.service.CpService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

@RequestMapping("/cpgl")
@Controller
public class CPGLController {

    @Resource
    CpService cpService;

    @RequestMapping("/list")
    public ModelAndView list() {
        List<Cp> cpList = cpService.list();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("cpList", cpList);
        modelAndView.setViewName("cpgl");
        return modelAndView;
    }


    @RequestMapping("/save")
    public String save(Cp cp) {
        cpService.save(cp);
        return "redirect:/cpgl/list";
    }

    @RequestMapping("/del/{Id}")
    public String del(@PathVariable("Id") Integer Id) {
        cpService.del(Id);
        return "redirect:/cpgl/list";
    }

}
