package com.six.controller;

import com.six.domain.Repari;
import com.six.service.RepariService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class CarController {

    @Autowired
    RepariService repariService;

    @RequestMapping("/repair/list")
    public ModelAndView repari() {
        List<Repari> repariList = repariService.findAll();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("repairList", repariList);
        modelAndView.setViewName("repair-list");
        return modelAndView;
    }

    @RequestMapping("/repair/del/{carNumber}")
    public String del(@PathVariable("carNumber") String carNum) {
        repariService.del(carNum);
        return "redirect:/repair/list";
    }
}
