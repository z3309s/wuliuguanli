package com.six.controller;

import com.six.domain.Log;
import com.six.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RequestMapping("/log")
@Controller
public class logController {
    @Autowired
    LogService logService;

    @RequestMapping("/list")
    public ModelAndView list() {
        ModelAndView modelAndView = new ModelAndView();
        List<Log> logList = logService.getALlList();
        //设置模型
        modelAndView.addObject("sysLogs", logList);
        //设置视图
        modelAndView.setViewName("syslog-list");
        return modelAndView;
    }

}
