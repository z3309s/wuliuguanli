package com.six.mapper;

import com.six.domain.Role;

import java.util.List;

public interface RoleMapper {
    List<Role> findAll();

    void save(Role role);

    List<Role> findRoleByUserId(Integer id);

    List<Role> serch(String rolename);

    void del(Integer roleId);
}
