package com.six.mapper;

import com.six.domain.Cp;
import com.six.domain.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CpMapper {
    List<Cp> findAll();

    Integer save(Cp cp);

    void del(Integer Id);
}
