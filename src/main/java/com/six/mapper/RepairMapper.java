package com.six.mapper;

import com.six.domain.Repari;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RepairMapper {
    List<Repari> findAll();

    void del(String carNum);
}
