package com.six.mapper;


import com.six.domain.User;
import com.six.domain.UserAndRole;
import org.apache.ibatis.annotations.Param;


import java.util.List;

public interface UserMapper {
    List<User> findAll();

    Integer save(User user);

    void saveUserRoleRel(@Param("userId") Integer id, @Param("roleId") Integer roleIds);

    void delUserRoleRel(Integer userId);

    void del(Integer userId);

    User getUser(String username);

    void addUserAndRole(UserAndRole userAndRole);

    int addUser(User user);

    int UserIsExisted(User user);

    List<User> serch(String username);
}
