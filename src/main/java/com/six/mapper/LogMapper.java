package com.six.mapper;

import com.six.domain.Log;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface LogMapper {
    List<Log> findAllList();

    void addLog(@Param("username") String username, @Param("desc") String desc);
}
