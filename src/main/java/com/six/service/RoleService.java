package com.six.service;

import com.six.domain.Role;

import java.util.List;

public interface RoleService {
    List<Role> list();

    void save(Role role);

    List<Role> serch(String rolename);

    void del(Integer roleId);
}
