package com.six.service;

import com.six.domain.User;

import java.util.List;

public interface UserService {
    List<User> list();

    void save(User user, Integer[] roleIds);

    void del(Integer userId);

    Boolean login(User user);

    int addUser(User user);

    boolean UserIsExisted(User user);

    List<User> serch(String username);
}
