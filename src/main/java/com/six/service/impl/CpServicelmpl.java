package com.six.service.impl;

import com.six.domain.Cp;
import com.six.domain.Role;
import com.six.domain.User;
import com.six.mapper.CpMapper;
import com.six.service.CpService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CpServicelmpl implements CpService {

    @Resource
    CpMapper cpMapper;

    public List<Cp> list() {
        List<Cp> cpList = cpMapper.findAll();
        //封装userList中的每一个User的roles数据

        return cpList;
    }

    public void save(Cp cp) {

        cpMapper.save(cp);
    }

    public void del(Integer Id) {

        cpMapper.del(Id);
    }
}
