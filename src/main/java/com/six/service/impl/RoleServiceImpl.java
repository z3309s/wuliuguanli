package com.six.service.impl;


import com.six.domain.Role;
import com.six.mapper.RoleMapper;
import com.six.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleMapper roleMapper;

    public List<Role> list() {
        List<Role> roleList = roleMapper.findAll();
        return roleList;
    }

    public void save(Role role) {
        roleMapper.save(role);
    }

    @Override
    public List<Role> serch(String rolename) {
        return roleMapper.serch(rolename);
    }

    @Override
    public void del(Integer roleId) {
        roleMapper.del(roleId);
    }
}
