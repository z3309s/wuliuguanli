package com.six.service.impl;

import com.six.domain.Repari;
import com.six.mapper.RepairMapper;
import com.six.service.RepariService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepariServiceImpl implements RepariService {
    @Autowired
    RepairMapper repairMapper;

    @Override
    public List<Repari> findAll() {
        return repairMapper.findAll();
    }

    @Override
    public void del(String carNum) {
        repairMapper.del(carNum);
    }
}
