package com.six.service.impl;

import com.six.domain.Log;
import com.six.mapper.LogMapper;
import com.six.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    LogMapper logMapper;

    @Override
    public List<Log> getALlList() {
        return logMapper.findAllList();
    }

    @Override
    public void addLog(String username, String desc) {
        logMapper.addLog(username, desc);
    }
}
