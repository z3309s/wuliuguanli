package com.six.service;

import com.six.domain.Repari;

import java.util.List;

public interface RepariService {
    List<Repari> findAll();

    void del(String carNum);
}
