package com.six.service;

import com.six.domain.Log;

import java.util.List;

public interface LogService {
    List<Log> getALlList();

    void addLog(String username, String desc);
}
