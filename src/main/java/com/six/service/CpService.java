package com.six.service;

import com.six.domain.Cp;
import com.six.domain.User;

import java.util.List;

public interface CpService {
    List<Cp> list();

    void save(Cp cp);

    void del(Integer Id);
}
